package com.fnotebook.views.fragments;

import androidx.fragment.app.Fragment;

/**
 * Created by Hemanth on 03-04-2020.
 * Updated by Hemanth on 03-04-2020.
 * <p>
 * Copyright (c) 2020 MouriTech. All rights reserved.
 * <p>
 * Basefragemnt is super class of all the Fragments
 */
public class BaseFragment extends Fragment {

}
